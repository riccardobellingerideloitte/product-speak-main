/**
 * Salesforce Adapter for Product Speak
 * Using variable sfAdapter after including JS file
 * Contact: Jiedong Ding, Deloitte Digital, Ireland
 * Last Updated: 29/03/2016
 */
var sfAdapter = (function() {

	/**
	 * Init the force library 
	 * @param isADevice - true if we are using a mobile device, false if we are using the browser 
	 * @accessToken - if already logged in this is the accessToken value 
	 * @instanceURL - if already logged in this is the instanceURL value 
	 */ 
	 function init(isADevice, accessToken, instanceURL, loginSuccessHandler, loginErrorHandler) {
	 	// init the app
		var initParams; 
		//If we are on a device 
		if (window.cordova) { 
			initParams = {appId: '3MVG92u_V3UMpV.iqZpdVLzxOcUZ5h9l3GpVKqoclEf2LnXuYFT5qUKOL9Bqz8N_qQ10L4_m_pozESjja70C.', 
				loginURL: 'https://test.salesforce.com', 
				oauthCallbackURL: 'https://test.salesforce.com/services/oauth2/success', 
				useProxy: false}; 			
		} else { 
			initParams = {appId: '3MVG92u_V3UMpV.iqZpdVLzxOcUZ5h9l3GpVKqoclEf2LnXuYFT5qUKOL9Bqz8N_qQ10L4_m_pozESjja70C.', 
				loginURL: 'http://test.salesforce.com', 
				oauthCallbackURL: 'http://localhost:8200/oauthcallback.html', 
				useProxy: true}; 
		} 
		if(accessToken) initParams.accessToken = accessToken; 
		if(instanceURL) initParams.instanceURL = instanceURL; 
		force.init(initParams); 
		
		if(!accessToken || !instanceURL) { 
			// login
			force.login(loginSuccessHandler, loginErrorHandler);
		} 
	 } 

	/**
	 * Get list of accounts by user ID
	 * @param id - user id
	 * @param successHandler - function to call back when succeeds
	 * @param errorHandler - function to call back when fails
	 */
	function getAccountsByUserId(id, successHandler, errorHandler) {
		soqlStr = 'select id, name from account where ownerId = ' + '\'' + id + '\'';
		force.query(
			soqlStr, 
			function(success) {
				if(successHandler) {
					successHandler(success.records);
				}
			}, 
			function(error) {
				if(errorHandler) {
					errorHandler(error);
				}
			}
		);
	}

	/**
	 * Get list of contacts by account ID
	 * @param id - account id
	 * @param successHandler - function to call back when succeeds
	 * @param errorHandler - function to call back when fails
	 */
	function getContactsByAccountId(id, successHandler, errorHandler) {
		soqlStr = 'select id, name from contact where accountId = ' + '\'' + id + '\'';
		force.query(
			soqlStr,
			function(success) {
				if(successHandler) {
					successHandler(success.records);
				}
			},
			function(error) {
				if(errorHandler) {
					errorHandler(error);
				}
			}
		);
	}

	/**
	 * Get list of opportunities by account ID
	 * @param id - account id
	 * @param successHandler - function to call back when succeeds
	 * @param errorHandler - function to call back when fails
	 */
	function getOpportunitiesByAccountId(id, successHandler, errorHandler) {
		soqlStr = 'select id, name, drug_product_name__c from opportunity where accountid = ' + '\'' + id + '\'';
		force.query(
			soqlStr,
			function(success) {
				if(successHandler) {
					successHandler(success.records);
				}
			},
			function(error) {
				if(errorHandler) {
					errorHandler(error);
				}
			}
		);
	}

	/**
	 * Create a meeting record on Meeting Object by contact Id and opportunity Id
	 * @param title - title of the meeting
	 * @param contactId - contact Id
	 * @param opportunityId - opportunity Id
	 * @param {Date} startDateTime - the start datetime of the meeting
	 * @param successHandler - function to call back when succeeds
	 * @param errorHandler - function to call back when fails
	 */
	function createMeeting(title, contactId, opportunityId, startDateTime, successHandler, errorHandler) {
		force.create(
			'Meeting__c',
			{
				'Title__c': title,
				'Contact__c': contactId,
				'Opportunity__c': opportunityId,
				'StartDateTime__c': startDateTime
			},
			function(success) {
				if(successHandler) {
					// return meeting id
					successHandler(success.id);
				}
			},
			function(error) {
				if(errorHandler) {
					errorHandler(error);
				}
			}
		);
	}

	/**
	 * Update a meeting with end time of the meeting
	 * @param id - meeting id
	 * @param {Date} endDateTime - the end datetime of the meeting
	 * @param successHandler - function to call back when succeeds
	 * @param errorHandler - function to call back when fails
	 */
	function updateMeeting(id, endDateTime, successHandler, errorHandler) {
		force.update(
			'Meeting__c',
			{
				'Id': id,
				'EndDateTime__c': endDateTime
			},
			function(success) {
				if(successHandler) {
					successHandler('Meeting updated successfully');
				}
			},
			function(error) {
				if(errorHandler) {
					errorHandler(error);
				}
			}
		);
	}

	/**
	 * Create an action
	 * @param title - title of the action
	 * @param meetingId - meeting id
	 * @param channel - channel type of the action
	 * @param successHandler - function to call back when succeeds
	 * @param errorHandler - function to call back when fails
	 */
	function createAction(title, meetingId, channel, successHandler, errorHandler) {
		force.create(
			'Action__c',
			{
				'Title__c': title,
				'Meeting__c': meetingId,
				'Channel__c': channel
			},
			function(success) {
				if(successHandler) {
					// return action id
					successHandler(success.id);
				}
			},
			function(error) {
				if(errorHandler) {
					errorHandler(error);
				}
			}
		);
	}

	// The public APIs
	return { 
		init: init, 
		getAccountsByUserId: getAccountsByUserId,
		getContactsByAccountId: getContactsByAccountId,
		getOpportunitiesByAccountId: getOpportunitiesByAccountId,
		createMeeting: createMeeting,
		updateMeeting: updateMeeting,
		createAction: createAction
    };

}()); 