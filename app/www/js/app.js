var wrapper = document.getElementById("signature-pad"),
    clearButton = document.querySelector("[data-action=clear]"),
    saveButton = document.querySelector("[data-action=save]"),
    canvas = wrapper.querySelector("canvas"),
    signaturePad;

// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

window.onresize = resizeCanvas;
resizeCanvas();

signaturePad = new SignaturePad(canvas);
resizeCanvas();


clearButton.addEventListener("click", function (event) {
    signaturePad.clear();
});

function submitEmail() { 
   //Save the image 
  if (window.XMLHttpRequest) {
        ajax = new XMLHttpRequest();
   } else if (window.ActiveXObject) {
        ajax = new ActiveXObject("Microsoft.XMLHTTP");
  }
  ajax.open('POST', 'https://evangreallycvcom.ipage.com/productSpeak/app/saveImage.php', false);
  ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  ajax.onreadystatechange = function() {
       console.log(ajax.responseText);
  }
  ajax.send("img="+signaturePad.toDataURL());
  console.log('saved'); 
  
  var pardotForm = document.getElementById("pardot-form"); 
  var w = window.open('about:blank','submit_to_pardot','toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=400,height=300,left = 312,top = 234');
  pardotForm.target = 'submit_to_pardot'; 

  pardotForm.submit(); 
}