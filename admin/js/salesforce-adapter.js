/**
 * Salesforce Adapter for Product Speak
 * Using variable sfAdapter after including JS file
 * Contact: Jiedong Ding, Deloitte Digital, Ireland
 * Last Updated: 29/03/2016
 */
var sfAdapter = (function() {

	/**
	 * Init the force library 
	 * @param isADevice - true if we are using a mobile device, false if we are using the browser 
	 * @accessToken - if already logged in this is the accessToken value 
	 * @instanceURL - if already logged in this is the instanceURL value 
	 */ 
	 function init(isADevice, accessToken, instanceURL, loginSuccessHandler, loginErrorHandler) {
	 	// init the app
		var initParams; 
		//If we are on a device 
		if (window.cordova) { 
			initParams = {appId: '3MVG92u_V3UMpV.iqZpdVLzxOcUZ5h9l3GpVKqoclEf2LnXuYFT5qUKOL9Bqz8N_qQ10L4_m_pozESjja70C.', 
				loginURL: 'https://test.salesforce.com', 
				oauthCallbackURL: 'https://test.salesforce.com/services/oauth2/success', 
				useProxy: false}; 			
		} else { 
			initParams = {appId: '3MVG92u_V3UMpV.iqZpdVLzxOcUZ5h9l3GpVKqoclEf2LnXuYFT5qUKOL9Bqz8N_qQ10L4_m_pozESjja70C.', 
				loginURL: 'http://test.salesforce.com', 
				oauthCallbackURL: 'http://localhost:8200/oauthcallback.html', 
				useProxy: true}; 
		} 
		if(accessToken) initParams.accessToken = accessToken; 
		if(instanceURL) initParams.instanceURL = instanceURL; 
		force.init(initParams); 
		
		if(!accessToken || !instanceURL) { 
			// login
			force.login(loginSuccessHandler, loginErrorHandler);
		} 
	 } 
	
	/* Admin Dashboard functions */ 
	/**
	 * returns an array with pairs: {channel type: #hits}
	 */
	function getChannelHits(successHandler, errorHandler) {
		soqlStr = "select COUNT(Id) TOTAL, Channel__c CHANNEL from Action__c GROUP BY Channel__c";
		force.query(
			soqlStr,
			function(success) {
				if(successHandler) {
					successHandler(success.records);
				}
			},
			function(error) {
				if(errorHandler) {
					errorHandler(error);
				}
			}
		);
	}

	/**
	 * returns an array with all the events saved in the Action table 
	 */
	function getEvents(successHandler, errorHandler) {
		soqlStr = "select Id, Name, CreatedById, Title__c, Meeting__c, Channel__c, CreatedDate from Action__c order by Name Desc";
		force.query(
			soqlStr,
			function(success) {
				if(successHandler) {
					successHandler(success.records);
				}
			},
			function(error) {
				if(errorHandler) {
					errorHandler(error);
				}
			}
		);
	}

	// The public APIs
	return { 
		init: init, 
		getChannelHits: getChannelHits, 
		getEvents: getEvents 
    };

}()); 